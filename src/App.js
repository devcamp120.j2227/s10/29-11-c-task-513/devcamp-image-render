import image from "./assets/images/devcampreact.png";

function App() {
  return (
    <div>
      <img src={image} alt="hello devcamp" width={1000}/>
    </div>
  );
}

export default App;
